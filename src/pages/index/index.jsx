import { Component } from 'react'
import { View } from '@tarojs/components'
import './index.css'
import Layout from "../../components/Layout/Layout"

export default class Index extends Component {

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    return (
      <View className='index'>
        <Layout></Layout>
      </View>
    )
  }

}
