import { Component } from 'react'
import { View,Text } from '@tarojs/components'
import './Keyboard.css'

export default class Keyboard extends Component {

  state = {
      amount:"0"
  }

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    const keys = [
        {key:"1",value:"1"},{key:"2",value:"2"},{key:"3",value:"3"},{key:"×",value:"*"},
        {key:"4",value:"4"},{key:"5",value:"5"},{key:"6",value:"6"},{key:"-",value:"-"},
        {key:"7",value:"7"},{key:"8",value:"8"},{key:"9",value:"9"},{key:"+",value:"+"},
        {key:"清除",value:"C"},{key:"0",value:"0"},{key:".",value:"."},{key:"=",value:"="},
    ]
    return (
      <View className='keyboard_container'>
          <View className="amount_container">
            <Text className="amount_lable">¥</Text>
            <Text className="amount">{this.state.amount}</Text>
          </View>
          <View className="keyBoard_container">
              <View className="keyBoard_center">
                {
                    keys.map(item=>{
                        return (
                            <View className="key" hoverClass="key_hover" key={item.value}>
                                <Text>{item.key}</Text>
                            </View>
                        )
                    })
                }
              </View>
          </View>
      </View>
    )
  }

}