import { Component } from 'react'
import { View,Text,Image } from '@tarojs/components'
import './MerchantInfo.css'

export default class MerchantInfo extends Component {
    constructor(props){
        super(props)
    }

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    return (
      <View className='mer_container'>
          <View className="icon_warp">
              <Image
                // src="../../img/shop-2.png"
                src="https://mobilepos.95516.com/upposhtml/pos/HUAWEI/img/shop-2.png"
                className="icon"
                mode="center"
              />
          </View>
          <View className="info_warp">
              <View className="mer_info">
                    <Text className="name">海底捞小店</Text>
                    <View className="role">
                        <View className="role_icon">
                            <Text>店主</Text>
                        </View>
                        <Text className="role_name">子豪</Text>
                    </View>
              </View>
          </View>
      </View>
    )
  }

}