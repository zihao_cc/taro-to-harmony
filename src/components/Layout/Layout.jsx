import { Component } from 'react'
import { View,Text } from '@tarojs/components'
import './Layout.css'
import Keyboard from "../KeyBoard/KeyBoard";
import MerchantInfo from "../MerchantInfo/MerchantInfo"

export default class Layout extends Component {

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    return (
      <View className='layout_container'>
          <MerchantInfo></MerchantInfo>
          <Keyboard></Keyboard>
      </View>
    )
  }

}